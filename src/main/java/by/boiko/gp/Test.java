package by.boiko.gp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        List<Integer> arrayList = new ArrayList<Integer>();
        List<String> lines = Files.readAllLines(Paths.get("input.txt"));
        String[] chs = lines.get(0).split(" ");
        lines.clear();
        for (String ch : chs) {
            int i = Integer.valueOf(ch);
            arrayList.add(i);
        }
        int firstNumber = arrayList.get(0);
        int secondNumber = arrayList.get(1);
        int summ = firstNumber + secondNumber;
        System.out.println(summ);
        String content = String.valueOf(summ);
        Files.write(Paths.get("output.txt"), content.getBytes());
    }

}