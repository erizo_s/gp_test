package by.boiko.gp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TestBuf {

    public static void main(String[] args) throws Exception {
        List<Integer> arrayList = new ArrayList<Integer>();
        BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
        String a = br.readLine();
        String[] chs = a.split(" ");
        for (String ch : chs) {
            int i = Integer.valueOf(ch);
            arrayList.add(i);
        }
        int firstNumber = arrayList.get(0);
        int secondNumber = arrayList.get(1);
        int summ = firstNumber + secondNumber;
        System.out.println(summ);
    }
}
